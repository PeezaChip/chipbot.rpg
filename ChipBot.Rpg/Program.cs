﻿using RPGPlugin.Core.Classes;
using RPGPlugin.Core.Static.Races;
using RPGPlugin.Core.Static.Specs;

Hero CreateHero(string name, string race, string spec)
{
    var r = RaceDescriptions.Races[race];
    var s = SpecsDescriptions.Classes[spec];
    return new Hero()
    {
        Name = name,
        RaceDescription = r,
        RaceReference = (Race)Activator.CreateInstance(r.Race),
        SpecDescription = s,
        SpecReference = (Spec)Activator.CreateInstance(s.Spec)
    };
}

DungeonHero CreateDungeonHero(string name, string race, string spec)
{
    return new DungeonHero(CreateHero(name, race, spec));
}

var rng = new Random();

var hero1 = CreateDungeonHero("hero1", "naga", "rogue");
var hero2 = CreateDungeonHero("hero2", "human", "warrior");

var fight = new Fight(new List<Creature> { hero1 }, new List<Creature> { hero2 }, Console.WriteLine, rng);

fight.DoFight();

Console.ReadKey();